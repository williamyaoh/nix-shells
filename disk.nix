# Utilities for working with disks -- partitioning, setting flags, etc.

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    parted
    gparted
    gnome3.gnome-disk-utility

    file
  ];
}
