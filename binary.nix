# Viewing, editing, and manipulating binary/executables.

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    chrpath
    binutils
  ];
}
