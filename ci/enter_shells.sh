#!/usr/bin/env sh

for shell in $(ls *.nix); do
    echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> attempting to enter shell '$shell'...";
    nix-shell "$shell" </dev/null;
    echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> successfully entered '$shell'.";
done
