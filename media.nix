# Viewing and editing media files.

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    vlc

    easytag             # mp3 metadata editing
    gimp                # raster image editing
    inkscape            # svg/vector image editing
    imagemagick         # command-line image manipulation

    sox                 # command-line audio conversion
    ffmpeg_4            # command-line video conversion
  ];
}
