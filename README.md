# shells

A lot of software we don't need to have installed all the time, or
only need installed for certain tasks. Additionally, certain
pieces of software tend to go together, like `gparted` and `gdisk`.
Accordingly, we bundle them together here so that they can be
installed and used by running `nix-shell` on the appropriate
Nix file.