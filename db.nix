# Utilities for working with disks -- partitioning, setting flags, etc.

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    sqlite
    postgresql
    mysql

    sqlint  ## Simple SQL linter
  ];
}
