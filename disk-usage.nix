# It is a crying shame that Linux's default choice of filesystem makes
# it difficult to do something as important as figure out how much disk
# space you have. Yes, obviously it's not something that you do that often,
# but it tends to be time-critical when you *do*.

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    ncdu   # ncurses app to view disk usage
           # cache a view using `ncdu -1o <cache file> /',
           # view it using `ncdu -f <cache file>'
           # still takes a long ass while, but it's something
           # you can do regularly in the background

    agedu  # like du, but also points out good files to remove
  ];
}
